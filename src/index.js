import Vue from 'vue'
import App from './App.vue'
import router from './routers'
import vuetify from './vuetify'
import 'babel-polyfill'
import './assets/styles/styles.css'

new Vue({
  el: '#app',
  router,
  vuetify,
  template: '<App/>',
  components: { App },
})
