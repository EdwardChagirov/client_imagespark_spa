import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../pages/Home.vue'
import Auth from '../pages/Auth.vue'
import Reg from '../pages/Reg.vue'
import Admin from '../pages/Admin.vue'
import NotFound from '../pages/NotFound.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/', component: Home },
  { path: '/auth', component: Auth },
  { path: '/register', component: Reg },
  { path: '/admin', component: Admin },
  { path: '/404', component: NotFound },
  { path: '*', redirect: '/404' },
]

const router = new VueRouter({
  routes,
})

export default router
