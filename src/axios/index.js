import axios from 'axios'
import { getServerPath } from '../helpers'

export function playerAdd(context) {
  axios
    .post(`${getServerPath}/player/add`, {
      position: context.position,
      name: context.name,
      secondName: context.secondName,
      age: context.age,
      rating: context.rating,
      image: context.image,
      imageName: context.imageName,
    })
    .then(res => {
      context.position = null
      context.name = null
      context.secondName = null
      context.age = null
      context.rating = null
      context.image = null
      context.imageName = null

      context.$refs.form.reset()
    })
    .catch(function(err) {
      console.log(err)
    })
}
